# Ansible Role: macOS Finder Configuration

Configure the macOS Finder using the defaults mechanism.

## Requirements

None

## Role Variables

None

## Dependencies

None

## Example Playbook

```yaml
- hosts: localhost
  roles:
    - refnode.macosfinder
```

## License

See `LICENSE` file.

## Author Information

For authors/contributors please check AUTHORS.md
